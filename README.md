[![build status](https://gitlab.com/piLights/backend/badges/master/build.svg)](https://gitlab.com/piLights/backend/commits/master)

You can find the app on Google Play [here](https://play.google.com/store/apps/details?id=de.grunicke.lighter)

# License
This project is licensed under the General Public License 3.
You can find a copy of it [here](LICENSE).