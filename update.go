package main

import (
	"errors"
	"fmt"
	"net/http"
	"runtime"

	"gitlab.com/piLights/backend/configuration"
	"gitlab.com/piLights/backend/logging"

	"crypto"
	"io/ioutil"

	"encoding/hex"

	"strings"

	"github.com/inconshreveable/go-update"
)

//UPDATEURL is the URL from which the updates for the OS and architecture are fetched from
const UPDATEURL = "https://dist.pilights.de/piLightsAPI_" + runtime.GOOS + "_" + runtime.GOARCH

var errFileNotFound = errors.New("File not found")

//startUpdate starts the updateProcess
func startUpdate() {
	fmt.Println("Starting update...")
	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- fmt.Sprintf("Downloading from %s", configuration.DioderConfiguration.UpdateURL)
	}

	err := updateBinary(configuration.DioderConfiguration.UpdateURL)
	if err != nil {
		logging.LogChan <- "Updating failed!"
		logging.FatalChan <- err
	}

	logging.LogChan <- "Updated successfully"
}

//updateBinary updates the executable binary
func updateBinary(url string) error {
	// Fetch the Hash-Sum
	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- fmt.Sprintf("Downloading verification-hash from %s.sha256", url)
	}
	checksumResponse, err := http.Get(url + ".sha256")
	if err != nil {
		return err
	}
	defer checksumResponse.Body.Close()

	if checksumResponse.StatusCode != 200 {
		return errFileNotFound
	}

	checksum, err := ioutil.ReadAll(checksumResponse.Body)
	if err != nil {
		return err
	}
	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- fmt.Sprintf("Got the hash: %s", string(checksum))
		logging.LogChan <- fmt.Sprintf("Downloading the binary from %s", url)
	}

	response, err := http.Get(url)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return errFileNotFound
	}

	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- "Got it. Starting the update..."
	}

	hexChecksum, err := hex.DecodeString(strings.TrimSpace(string(checksum)))
	if err != nil {
		return err
	}

	err = update.Apply(response.Body, update.Options{
		Hash:     crypto.SHA256,
		Checksum: hexChecksum,
	})

	return err
}
