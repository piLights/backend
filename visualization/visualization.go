package visualization

import (
	"fmt"
	"net"

	"gitlab.com/piLights/backend/configuration"
	"gitlab.com/piLights/backend/logging"
)

// Visualizer represents the listener and parser for the visualizations
type Visualizer struct {
	listener  *net.UDPConn
	musicChan chan []byte
}

// NewVisualizer creates a new Visualizer
func NewVisualizer() (*Visualizer, error) {
	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- "Initialisation of the visualizer..."
	}
	visualizer := &Visualizer{}

	visualizer.musicChan = make(chan []byte, 10000)

	protocol := "udp"

	if configuration.DioderConfiguration.IPv4Only {
		protocol = "udp4"
	}

	if configuration.DioderConfiguration.IPv6Only {
		protocol = "udp6"
	}

	address, err := net.ResolveUDPAddr(protocol, configuration.DioderConfiguration.Visualizer.BindTo)
	if err != nil {
		return nil, err
	}

	visualizer.listener, err = net.ListenUDP(protocol, address)
	if err != nil {
		return nil, err
	}

	return visualizer, nil
}

// Start starts the Visualizer created with NewVisualizer(). Start() should be called in a seperate goroutine
func (v *Visualizer) Start() {
	//go v.visualize()

	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- "Starting the visualizer"
	}

	buf := make([]byte, 1024)

	// Loop forever
	for {
		n, addr, err := v.listener.ReadFromUDP(buf)
		if err != nil {
			logging.LogChan <- err
		}

		//v.musicChan <- buf

		if configuration.DioderConfiguration.Debug {
			logging.LogChan <- fmt.Sprintf("Visualizer: Recieved %d Byte from %s", n, addr.String())
		}
	}
}

func (v *Visualizer) visualize() {
	for buf := range v.musicChan {
		max, min := buf[0], buf[0]
		var average int

		for _, value := range buf {
			average += int(value)
			if value < min {
				min = value
			}

			if value > max {
				max = value
			}
		}

		logging.LogChan <- fmt.Sprintf("%v %v %v", min, max, average/len(buf))
	}
}
