package rpc

import (
	"golang.org/x/net/context"

	"gitlab.com/piLights/backend/configuration"
	"gitlab.com/piLights/backend/piLightsVersion"
	"gitlab.com/piLights/proto"
)

type systemServer struct{}

func (s *systemServer) ChangeServerParameter(ctx context.Context, changeParameterMessage *LighterGRPC.ChangeParameterMessage) (*LighterGRPC.Confirmation, error) {
	if !checkAccess(changeParameterMessage) {
		return nil, errNotAuthorized
	}

	return nil, errNotImplemented
}

func (s *systemServer) LoadServerConfiguration(ctx context.Context, request *LighterGRPC.Request) (*LighterGRPC.ServerConfiguration, error) {
	ipVersion := &LighterGRPC.IPVersion{
		Version: LighterGRPC.IPVersion_DUAL,
	}

	if configuration.DioderConfiguration.IPv4Only {
		ipVersion.Version = LighterGRPC.IPVersion_IPV4ONLY
	}

	if configuration.DioderConfiguration.IPv6Only {
		ipVersion.Version = LighterGRPC.IPVersion_IPV6ONLY
	}

	config := &LighterGRPC.ServerConfiguration{
		BindTo:            configuration.DioderConfiguration.BindTo,
		ConfigurationFile: configuration.DioderConfiguration.ConfigurationFile,
		Debug:             configuration.DioderConfiguration.Debug,
		PiBlaster:         configuration.DioderConfiguration.PiBlaster,
		ServerName:        configuration.DioderConfiguration.ServerName,
		UpdateURL:         configuration.DioderConfiguration.UpdateURL,
		Pins: &LighterGRPC.Pins{
			BluePin:  int32(configuration.DioderConfiguration.Pins.Blue),
			GreenPin: int32(configuration.DioderConfiguration.Pins.Green),
			RedPin:   int32(configuration.DioderConfiguration.Pins.Red),
			WhitePin: 0,
		},
		SoftwareVersion: piLightsVersion.Version,
		Visualizer: &LighterGRPC.Visualizer{
			BindTo:  configuration.DioderConfiguration.Visualizer.BindTo,
			Enabled: configuration.DioderConfiguration.Visualizer.Enabled,
		},
		FadeTime:  &LighterGRPC.FadeTime{Milliseconds: 0}, // @Todo
		IpVersion: ipVersion,
	}

	return config, nil
}

func (s *systemServer) SetServerConfiguration(ctx context.Context, serverConfiguration *LighterGRPC.ServerConfiguration) (*LighterGRPC.Confirmation, error) {
	if !checkAccess(serverConfiguration) {
		return nil, errNotAuthorized
	}

	if serverConfiguration.BindTo != configuration.DioderConfiguration.BindTo {
		configuration.DioderConfiguration.BindTo = serverConfiguration.BindTo
	}

	if serverConfiguration.Debug != configuration.DioderConfiguration.Debug {
		configuration.DioderConfiguration.Debug = serverConfiguration.Debug
	}

	if serverConfiguration.ServerName != configuration.DioderConfiguration.ServerName {
		configuration.DioderConfiguration.ServerName = serverConfiguration.ServerName
	}

	//@ToDo: Reload all stuff, that needs to be reloaded
	// This includes:
	//	* Save the new configuration
	//	* Reload the GRPC-Stub to listen on the new interface
	//	* Reload the dioder-Instance to write to the new FIFO & Pins

	return &LighterGRPC.Confirmation{Success: true}, nil
}

func (s *systemServer) Version(ctx context.Context, request *LighterGRPC.Empty) (*LighterGRPC.BackendVersion, error) {
	if !checkAccess(request) {
		return nil, errNotAuthorized
	}

	return &LighterGRPC.BackendVersion{
		VersionCode:     piLightsVersion.Version,
		UpdateAvailable: false, // @ToDo!
	}, nil
}
