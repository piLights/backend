package rpc

import (
	"image/color"

	"gitlab.com/piLights/proto"
)

func convertColorMessage(colorMessage *LighterGRPC.ColorMessage) color.RGBA {
	red := uint8(colorMessage.R)
	green := uint8(colorMessage.G)
	blue := uint8(colorMessage.B)

	return color.RGBA{
		R: red,
		G: green,
		B: blue,
	}
}
