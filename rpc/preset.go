package rpc

import (
	"fmt"

	"gitlab.com/piLights/backend/configuration"
	"gitlab.com/piLights/backend/logging"
	"gitlab.com/piLights/proto"
	"golang.org/x/net/context"
)

func (s *lighterServer) AddPreset(ctx context.Context, lighterPreset *LighterGRPC.Preset) (*LighterGRPC.Confirmation, error) {
	if !checkAccess(lighterPreset) {
		return nil, errNotAuthorized
	}

	preset := Preset(*lighterPreset)

	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- fmt.Sprintf("AddPreset: %+v", preset)
	}

	confirmation := &LighterGRPC.Confirmation{
		Success: true,
	}

	err := preset.GenerateJob()
	if err != nil {
		confirmation.Success = false

		if configuration.DioderConfiguration.Debug {
			logging.LogChan <- fmt.Sprintf("Job scheduling failed: %s", err)
		}

		return nil, err
	}

	err = configuration.DatabaseInstance.Save(&preset)
	if err != nil {
		logging.LogChan <- err
		confirmation.Success = false
	}

	return confirmation, err
}

func (s *lighterServer) GetPresetList(request *LighterGRPC.Request, stream LighterGRPC.RgbService_GetPresetListServer) error {
	if !checkAccess(request) {
		return errNotAuthorized
	}

	var presetList []LighterGRPC.Preset
	err := configuration.DatabaseInstance.All(&presetList)
	if err != nil {
		logging.LogChan <- err
	}

	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- fmt.Sprintf("GetPresetList: Sending %d presets", len(presetList))
	}

	for _, preset := range presetList {
		stream.Send(&preset)
	}

	return nil
}

func (s *lighterServer) UpdatePreset(ctx context.Context, lighterPreset *LighterGRPC.Preset) (*LighterGRPC.Confirmation, error) {
	if !checkAccess(lighterPreset) {
		return nil, errNotAuthorized
	}

	preset := Preset(*lighterPreset)

	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- fmt.Sprintf("UpdatePreset: %+v", preset)
	}

	confirmation := &LighterGRPC.Confirmation{
		Success: true,
	}

	err := configuration.DatabaseInstance.Update(&preset)
	if err != nil {
		logging.LogChan <- err
		confirmation.Success = false
	}

	return confirmation, err
}

func (s *lighterServer) RemovePreset(ctx context.Context, lighterPreset *LighterGRPC.Preset) (*LighterGRPC.Confirmation, error) {
	if !checkAccess(lighterPreset) {
		return nil, errNotAuthorized
	}

	preset := Preset(*lighterPreset)

	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- fmt.Sprintf("RemovePreset: %+v", preset)
	}

	confirmation := &LighterGRPC.Confirmation{
		Success: true,
	}
	err := configuration.DatabaseInstance.DeleteStruct(&preset)
	if err != nil {
		logging.LogChan <- err
		confirmation.Success = false
	}

	return confirmation, err
}
