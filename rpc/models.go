package rpc

import (
	"fmt"

	"gitlab.com/piLights/backend/configuration"
	"gitlab.com/piLights/backend/logging"
	"gitlab.com/piLights/cron"
	"gitlab.com/piLights/proto"
)

// Preset represents an Preset
type Preset struct {
	Id              int32 `storm:"id,unique,increment"`
	PresetName      string
	ColorMessage    *LighterGRPC.ColorMessage
	ScheduledSwitch *LighterGRPC.ScheduledSwitch
	EntryID         int32 `storm:"index,unique"`
}

func (preset *Preset) GenerateJob() error {
	if preset.ScheduledSwitch != nil {
		var err error

		schedulerJob := func(id cron.EntryID) {
			if configuration.DioderConfiguration.Debug {
				logging.LogChan <- fmt.Sprintf("Running scheduled job %d", id)
			}

			preset := Preset{}
			err := configuration.DatabaseInstance.One("EntryID", id, &preset)
			if err != nil {
				logging.LogChan <- fmt.Sprintf("Cron: Could not gather data for job %d", id)
				return
			}

			colorSet := convertColorMessage(preset.ColorMessage)

			configuration.DioderInstance.SetAll(colorSet)

			// Remove the preset if it should only run once
			if preset.ScheduledSwitch.Once {
				configuration.CronInstance.Remove(cron.EntryID(preset.EntryID))

				preset.ScheduledSwitch = nil

				err = configuration.DatabaseInstance.Update(&preset)
				if err != nil {
					logging.LogChan <- fmt.Sprintf("Remove preset: %v", err)
				}
			}
		}

		// Construct the Cron-Spec
		jobScheduledTimeString := "0"
		jobScheduledTimeString += " " + preset.ScheduledSwitch.Time.Minute
		jobScheduledTimeString += " " + preset.ScheduledSwitch.Time.Hour
		jobScheduledTimeString += " " + preset.ScheduledSwitch.Time.DayOfMonth
		jobScheduledTimeString += " " + preset.ScheduledSwitch.Time.Month
		jobScheduledTimeString += " " + preset.ScheduledSwitch.Time.DayOfWeek

		logging.LogChan <- jobScheduledTimeString

		entryID, err := configuration.CronInstance.AddFunc(jobScheduledTimeString, schedulerJob)
		if err != nil {
			logging.LogChan <- err
			return err
		}

		preset.EntryID = int32(entryID)
	}

	// The preset has no time specified and no error occured
	return nil
}
