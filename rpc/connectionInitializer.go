package rpc

import (
	"golang.org/x/net/context"

	"gitlab.com/piLights/backend/configuration"
	"gitlab.com/piLights/proto"
)

type connectionInitializer struct{}

func (s *connectionInitializer) LoadType(ctx context.Context, _ *LighterGRPC.Empty) (*LighterGRPC.ServiceTypeMessage, error) {
	// @ToDo: Dyanmic Type
	return &LighterGRPC.ServiceTypeMessage{ServiceType: LighterGRPC.ServiceType_RGB}, nil
}

func (s *connectionInitializer) LoadExternalConnection(ctx context.Context, _ *LighterGRPC.Empty) (*LighterGRPC.ExternalConnectionDetails, error) {
	return &LighterGRPC.ExternalConnectionDetails{
		Address: configuration.DioderConfiguration.ExternalConnection.Address,
		Port:    int32(configuration.DioderConfiguration.ExternalConnection.Port),
	}, nil
}
