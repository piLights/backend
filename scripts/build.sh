#!/bin/bash

DATE=$(date "+%s")

gox -output "dist/piLightsAPI_{{.OS}}_{{.Arch}}" -parallel=2 -verbose -os="${OPERATING_SYSTEM}" \
    -ldflags="-s -w -X gitlab.com/piLights/backend/piLightsVersion.Version=$CI_BUILD_REF -X gitlab.com/piLights/backend/piLightsVersion.BuildDate={DATE}"
