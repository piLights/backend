package main

import (
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/piLights/backend/configuration"
	"gitlab.com/piLights/backend/logging"
	"gitlab.com/piLights/backend/piLightsVersion"
	"gitlab.com/piLights/backend/rpc"
	"gitlab.com/piLights/dioder"

	"strconv"
	"time"

	"github.com/asdine/storm"
	"gitlab.com/piLights/backend/visualization"
	"gitlab.com/piLights/cron"
	"gitlab.com/piLights/proto"
	"gopkg.in/urfave/cli.v1"
)

// logger is the system-service logger
var compileTime time.Time

func init() {
	compileTimeInUnixSeconds, err := strconv.ParseInt(piLightsVersion.BuildDate, 10, 64)
	if err == nil {
		compileTime = time.Unix(compileTimeInUnixSeconds, 0)
	}
}

func main() {
	// Report crashes
	//defer captureCrash()

	application := cli.NewApp()
	application.Name = "piLights backend"
	application.Version = piLightsVersion.Version
	application.Flags = applicationFlags
	application.Compiled = compileTime
	application.Authors = []cli.Author{
		cli.Author{
			Name:  "Jannick Fahlbusch",
			Email: "contact@pilights.de",
		},
	}

	// Start the backend
	application.Action = startBackend

	application.Run(os.Args)

	//Handle CTRL  + C
	osSignalChan := make(chan os.Signal, 1)
	signal.Notify(osSignalChan, os.Interrupt)
	go func() {
		<-osSignalChan

		shutDown()
	}()
}

func startBackend(c *cli.Context) error {
	var err error

	logging.NewLoggingService()
	go logging.Service()

	//Check, if we should update
	if c.Bool("update") {
		startUpdate()
		return nil
	}

	if c.String("writeConfiguration") != "" {
		fmt.Println(configuration.DioderConfiguration)
		return configuration.DioderConfiguration.WriteConfigurationToFile(c.String("writeConfiguration"))
	}

	if c.String("configurationFile") != "" {
		config, err := configuration.NewConfiguration(c.String("configurationFile"))
		if err != nil {
			return err
		}

		configuration.DioderConfiguration = config
	}

	//Set the pins
	if configuration.DioderConfiguration.Debug {
		logging.LogChan <- fmt.Sprintf("Starting the backend. Version %s built on %s", piLightsVersion.Version, compileTime.String())
		logging.LogChan <- fmt.Sprintf("Configuring the Pins to: Red: %d, Green: %d, Blue: %d", configuration.DioderConfiguration.Pins.Red, configuration.DioderConfiguration.Pins.Green, configuration.DioderConfiguration.Pins.Blue)
	}

	if !configuration.DioderConfiguration.NoAutoconfiguration {
		go startAutoConfigurationServer()
	}

	if configuration.DioderConfiguration.Visualizer.Enabled {
		visualizer, err := visualization.NewVisualizer()
		if err != nil {
			logging.FatalChan <- err
		}

		go visualizer.Start()
	}

	configuration.DioderInstance = dioder.New(dioder.Pins{Red: configuration.DioderConfiguration.Pins.Red, Green: configuration.DioderConfiguration.Pins.Green, Blue: configuration.DioderConfiguration.Pins.Blue}, configuration.DioderConfiguration.PiBlaster)
	configuration.CronInstance = cron.New()
	configuration.CronInstance.Start()

	configuration.DatabaseInstance, err = storm.Open(configuration.DioderConfiguration.DatabasePath)
	if err != nil {
		logging.FatalChan <- err
	}

	go schedulePresets()

	rpc.StartServer()

	defer shutDown()

	return nil
}

// schedulePresets indexes all Presets and enqueues them in the scheduler
func schedulePresets() {
	var presetList []LighterGRPC.Preset
	err := configuration.DatabaseInstance.All(&presetList)
	if err != nil {
		logging.LogChan <- err
	}

	for _, lighterPreset := range presetList {
		preset := rpc.Preset(lighterPreset)
		err := preset.GenerateJob()
		if err != nil {
			logging.LogChan <- err
		}
	}
}

func shutDown() {
	//Stop the job-scheduler
	configuration.CronInstance.Stop()

	// Close channels
	close(logging.LogChan)
	close(logging.FatalChan)

	// Release all Pins for further use
	configuration.DioderInstance.Release()
	configuration.DatabaseInstance.Close()

	os.Exit(0)
}
