package piLightsVersion

/*
This is just a metapackage to keep the Version-Code in one place
*/

// Version of the backend
var (
	Version = "debugVersion"

	//This needs to be a string, we cannot replace it via LDFlags -X otherwise
	BuildDate = "1481916235"
)
